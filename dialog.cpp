﻿#include "dialog.h"
#include "ui_dialog.h"

#include <QMessageBox>
#include <database.h>
#include <lmbcs.h>

Dialog::Dialog(QWidget *parent)
  : QDialog(parent)
  , ui_(new Ui::Dialog)
{
  ui_->setupUi(this);

  connect(ui_->buttonBox->button(QDialogButtonBox::Close)
          , &QPushButton::clicked
          , this, &Dialog::close
          );

  connect(ui_->pathLineEdit, &QLineEdit::textChanged
          , this, &Dialog::pathChanged
          );

  connect(ui_->getTitleButton, &QPushButton::clicked
          , this, &Dialog::getTitle
          );
}

Dialog::~Dialog()
{
  delete ui_;
}

void Dialog::pathChanged(const QString &path)
{
  ui_->getTitleButton->setEnabled(!path.isEmpty());
}

void Dialog::getTitle()
{
  ntlx::Status status;
  ntlx::Database db(ui_->pathLineEdit->text()
                    , ui_->serverLineEdit->text()
                    , QString()
                    , &status);
  if (status.failure())
    QMessageBox::critical(this
                          , tr("Error")
                          , ntlx::Lmbcs(status).toQString()
                          );

  QString title = db.getTitle();
  ui_->titleLineEdit->setText(title);
}
