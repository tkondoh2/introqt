﻿#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
  Q_OBJECT

public:
  explicit Dialog(QWidget *parent = 0);
  ~Dialog();

public slots:
  void pathChanged(const QString& path);
  void getTitle();

private:
  Ui::Dialog* ui_;
};

#endif // DIALOG_H
