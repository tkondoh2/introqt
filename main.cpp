﻿#include "dialog.h"
#include <QApplication>

#include <QtDebug>
#include <lmbcs.h>

int main(int argc, char *argv[])
{
  ntlx::Status status = NotesInitExtended(argc, argv);
  if (status.failure())
    return -1;
  else
    qDebug() << ntlx::Lmbcs(status).toQString();

  QApplication a(argc, argv);
  Dialog w;
  w.show();

  int result = a.exec();

  NotesTerm();

  return result;
}
